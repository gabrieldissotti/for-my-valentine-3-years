//######################   FUNCTIONS   ##############################
//
function playOnReady(){
    //Delay
    setTimeout(function() {
        //esconder pre-loader
        $("div.progress").addClass("hide");
        //adicionar background
        $("body").css("background-image", "url(img/hearts.gif)"); 
        audio_body.play();
        //iniciar slider
        $('.slider').slider({interval: 13000,indicators: false,transition: 1000,height: 1280,});
        //posicionar scrol
        $('html, body').animate({ scrollTop: 0 }, 0,);
        //inciar animação de rolagem de scroll
        $('html, body').animate({ scrollTop: 0 }, 0,);
        $('html, body').animate({ scrollTop: 560 }, 13000,);
        refreshIntervalId = setInterval(function(){ 
            $('html, body').animate({ scrollTop: 0 }, 0,);
            $('html, body').animate({ scrollTop: 560 }, 13000,)
        }, 14000);

        //iniciando tooltipos do materilize para rodar toasts
        $('.tooltipped').tooltip({delay: 50});

        //determinando mensagem (toast)
        Materialize.toast('Audio: John Waller - The Marriage Prayer (Oração do Casamento)', 4000,'');

        //finalizar
        setTimeout(function(){        
            clearInterval(refreshIntervalId);
            audio_body.pause(); 
            $('.slider').slider('pause');
            Materialize.toast('Autor: Gabriel Dissotti', 6000,'');
            //delay para retirar conteudo da tela
            setTimeout(function() {
                //esconder slider
                $("ul.slides").addClass("hide");
                //remover gif de fundo:
                $("body").css("background-image", "none"); 
                //reposicionar scroll no topo
                $('html, body').animate({ scrollTop: 0 }, 0,);
            }, 4000);            
        }, 251000); //tempo para finalizar tudo
    }, 5000); //delay inicial

    //configs chrome to android
    webView.setWebChromeClient(new WebChromeClient());
    webView.getSettings().setJavaScriptEnabled(true);
    webView.getSettings().setDomStorageEnabled(true);
    webView.getSettings().setMediaPlaybackRequiresUserGesture(false);
}
//
//
//
//##############     MAIN    ############
$(document).ready(function(){    
    //iniciando conteudo
    playOnReady();   
});